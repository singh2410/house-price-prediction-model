#!/usr/bin/env python
# coding: utf-8

# # House Price Prediction Model using Machine Learning
# #By- Aarush Kumar
# #Dated: June 16,2021

# In[1]:


import pandas as pd
import matplotlib 
import seaborn as sns


# In[2]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/House Price Prediction/kc_house_data.csv')


# In[3]:


df


# In[4]:


df.head()


# ## Exploratory Data Analysis

# In[5]:


df.shape


# In[6]:


df.size


# In[7]:


df.info()


# In[8]:


df.isnull().sum()


# In[9]:


df.describe()


# ## Splitting Data into Training & Testing 

# In[11]:


df.columns


# In[13]:


X=df[['bedrooms', 'bathrooms', 'sqft_living',
       'sqft_lot', 'floors', 'waterfront', 'view', 'condition', 'grade',
       'sqft_above', 'sqft_basement',
        'sqft_living15', 'sqft_lot15']]


# In[14]:


X


# In[15]:


Y=df['price']


# In[16]:


Y


# In[17]:


from sklearn.model_selection import train_test_split


# In[18]:


X_train, X_test, Y_train, Y_test= train_test_split(X,Y,test_size=0.25, random_state=101)


# In[19]:


X_train


# In[20]:


X_test


# In[21]:


Y_train


# In[22]:


Y_test


# ## Standarization

# In[23]:


from sklearn.preprocessing import StandardScaler
std=StandardScaler()


# In[24]:


X_train_std=std.fit_transform(X_train)
X_test_std=std.transform(X_test)


# In[25]:


X_train


# In[26]:


X_train_std


# In[29]:


X_test


# In[28]:


X_test_std


# In[30]:


Y_train


# In[31]:


Y_test


# ## Model Training

# In[32]:


from sklearn.linear_model import LinearRegression
lr=LinearRegression()


# In[33]:


lr.fit(X_train_std,Y_train)


# In[34]:


Y_pred=lr.predict(X_test_std)


# In[35]:


Y_pred


# In[36]:


Y_test


# In[37]:


from sklearn.metrics import mean_absolute_error,r2_score


# In[38]:


mean_absolute_error(Y_test,Y_pred)


# In[39]:


X_test


# In[40]:


X_test.loc[7148]


# In[41]:


r2_score(Y_test,Y_pred)*100


# ## Testing by Predicting

# In[42]:


new_house=[[3,1,1520,5000,1,0,0,3,8,1000,1,2000,5000]]


# In[43]:


new_house_std=std.transform(new_house)


# In[44]:


new_house_std


# In[45]:


int(lr.predict(new_house_std))

